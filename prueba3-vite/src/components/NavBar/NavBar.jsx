import { Link, NavLink } from 'react-router-dom'
import carrito from '../../assets/react.svg'
import { Container, Nav, Navbar, NavDropdown } from 'react-bootstrap'

import './NavBar.css'


const NavBar = (   ) => {
    // console.log('Rendering Navbar')
    return (
        <>
            <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
                <Container>
                    <NavLink className={ ( {isActive} )=> isActive ? 'btn btn-success' : 'btn btn-outline-success'} to='/node_modules'>React-Bootstrap</NavLink>
                    <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                    <Navbar.Collapse id="responsive-navbar-nav">
                    <Nav className="me-auto">
                        <NavLink 
                            className={ ( {isActive} )=> isActive ? 'btn btn-success' : 'btn btn-outline-success'} 
                            to="/categoria/gorras"
                        >
                                Gorras
                        </NavLink>
                        <NavLink 
                            className={ ( {isActive} )=> isActive ? 'btn btn-success' : 'btn btn-outline-success'} 
                            to="/categoria/remeras"
                        >
                            Remeras
                        </NavLink>
                        
                    </Nav>
                    <Nav>
                        {/* <Nav.Link href="#deets">More deets</Nav.Link> */}
                        <Link to='/cart' >
                            {/* <img src={carrito} alt='imagen'/> */}
                            {/* <img className='w-25' src={'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAANgAAADpCAMAAABx2AnXAAAAh1BMVEX///8AAAD+/v77+/sEBAS8vLzy8vImJiabm5uvr68qKiqLi4vExMTu7u53d3e3t7c/Pz/T09Nvb2+BgYHCwsIMDAzm5uY+Pj4uLi5qamqioqLW1tbLy8tERETh4eF0dHSpqalbW1uUlJQWFhZlZWVTU1M2NjYeHh5+fn6Pj49aWlpMTEwZGRkazkpMAAAR6UlEQVR4nO1di2KquhINCUYsAqKI5SE+ilZr///7bjIJIVFsu0/BRy/r7GM1gmQxyWQyMwkI9ejRo0ePHj169OjRo0ePHj169OjRo0eP/19g+MdesPj7Z4AJQYSzw5jcuy7tIk4G7JX8NZERlFvzXY641PBfIoaRYzEsQwoN8i/hYFk2o5ZE9N41aRe4tGxOzHpbjSjokD/RIjHyTlaFtzIgSKhI9NQdDhiEe6vG/BQxQowaeWrVD8RGn5aBRe4hPqI9s8SQUooKvLuVgxjhJ9eRzNpwTYFxDTkv1wQ98WgNLdErL3gBtRGRGvIJ+QGx4iTYnMNeDoQB+YTEBLLpu+ha9jm/IRuyOTFuHj8lv2A6rxuhgY8gRtARn1FHsip7wcZqbpBWmVIw+59wWAO1Hg+GDd0MXjdrbh4/HzFoZowcPb41CAzYHTz0lJYIzC8ZP8/dX2EWPiMtoRaE0RvOhjUbDc4TK30E5Ei0EqRsS85lABP61F4DGK9pvjuXGG+Lz6juBeQEjDs+QEHa2nBtW9PnFRhW/xNE0pPREtm75/UaSIlwI4NJLXZeNWbsTfC8IkNYWfN8yB7ofcy2ynvXrgWAAiR0aaiPfaEa613q09KFeYMcmXrRuZ9e5G2ovV8jxd4gVnqyod4Drd1U/lvUdBm8jNv56f+ENgVG0ODdYHYY3AsRba0X8Akz9jZGL3sb3gnvZdGe0oL+urUMZnfDgbRmg3NaGIH6uCOhCk6bzlvMNf5K+njui/egXe0BEcF7k+JYZO0REyMHRu+XLrnbY0dwa30MehnT+Kl1/5ZordoMjmMRkfDe78/LSttUHcL0JGRmTqXf5rZtz+XLHLysb+KzDWUWOLrsuaWVicN4wbwugl+d16fyU6CI/Rz/j/1VVw1R634JjMaGwD7dsY6EsR5G40grCvjkuxyPo6qQvXH4zUnrY1jR4INPGPh77dQ5K1vlERRG+cKSveCTolaNYMEsNkMxW+07go7syq/mvaQvrGxm/gjo1sKsGxtIrIl5GOUXcNTHT0v63CeoRdUhwX4v1VuitSuqS7BhjgAxpmPU5JvN4l6BGKlpEBxV7Uk7dSprrJV6b6xsK4MgqJATeNs6gLHQdlvEmT7ftOyg6si8di4nVk+8eZE3hKqAd1nOOITEQkS0OQjhrXiJCOTMyFKPs3BE+BujtbpmJLi2S4zd+YMhslnl1OEXc4XEtGkTYhKzmMSwqjCr6LiSWMWWCGITEKw6zLOhKQJXghI1gnpdNEV2hcFnTcsCDwEWvAh2RR/TroplH6urgrn9YgmJ1b+qmmJ9HPLmIDFxT9CiUsdDgtqzgVU9eZRlWXmG4f8AwYUQ7zx8Jvpq6ARFTC8cnxHjk70pNEWtvjUx6NrFsLqVZRehOd5VmPrQHYybiteXxLRCXElMq10zMbuWGBrNq0umnXhaGDFCDYXP9Tb+VmK/JjZT1wvb1hviarx7Tw2RbRH6b8S0su+IIaqGz5cWZ89aBWAACQ2J7Svt2yWxapCxwTnWPjExRNGTYTCG0s/XJbFAqWK3o3QF4DCy9MnLQd5CzEwq64pJZRRe9jGMBTEDUisCHHXFUWd+Wva72UJvi5OCAjxwPL567E2NmBM7aEWeRyEKkMMZqhAsD15SH5mBSUV4UTyVDdEa5t2wQmIodnWJvZU+x2p14CPc3PcPvsLqwG/7ZMbeVUUHf8NPTuoSjj0r+5yx49WZvs9//DSDDyJAx047ZZ0R4yJrSpX4BzTNVb+Zv1Zfr2h3AQNun270qtgNFdBKGooaCu3G4y4Kjp3Gd85TGl8WkwX7N4EGM2FvKiyWe165d61sMVlCcGO/XFRlC/aOa705vK9P5ocN4du3ivbbqFtiOH41AtJSc4tpiwHCD3TNslBMNA00aEXCu2fK34nQHNjcndgdCsJ1WkvsMxMTDTGDNsx7pe61MlD3WeM4VutyLKYtW7CA1S08dTI8a/U4sz5S8h8sj5/aigQFiljScbSRXexFF9mCV7wjYuwu1nPbbnUHpPTpgVvWsPgEvjOTqh5cxh1HvRkNTxvKbO6c7Y5YPU+yadcSOw/cvtPujGCViGHzmUSnOfKg48bz6nIco46IbdlhlfcZEp26zVTgTp3KdSoCZlxd/czn8ZXy0M9UygOdlMRG3S5q4M2BMVvrNs8i+6kz51+JYTZBqFC07yk958ZoaK5TWzgAK2Ka++miKbK2VHmp6uPqplg7gitiKFAZvG8duIDPaXHflK/rxakniRkXVn1M7zxfSKxBecD0VeCDdJwVD/Foduf1ZOhhrvqYnkCjiGmFyhNce4dridWHVX1sqq6xJaTblgiZzuwKS936cCoXt145Uvcx5c+vHab4J8SKepneGHW8ckg44okRebFOhYq2aPDANYCMMkEMG2WCmHmqIFZPat+L26ysAZNAE5l/3LGPb0dXw9H95Db5VivYutC2/K1bl7pb7pofbtnXx/pIiFM5pRpTdvGN1i9jZKgPq2GVz5WyBjRPoKuRGXCg7eUpfs1Ln7zI+l/Ur6HCzWVXGavv1uhWS4WIt9Mn0vYV4fwLjYvD5uroeScRv0ZeGNeTF8aq5J6QvXkM3V+6BuQM2mhWK2l56JNvkFLdKJYZvtHuB0xNG9bH28o6C66zScDw0giuxrE6xodhsFqiejsKJhuIQad1ykzSrVtAqyGvkKu1K5snar7WYygfEiQxrBVeEKssDzMdAIOLe5uon3dluskNwAylaHjWYdqatiAEzpyZSkS20xsJDEFUiW7OlEHLxDZKKb5ENyMGCw0M6+OfiH3tzJHEPtVNW96qi0GFTN9HmxLDgljdGjads6kvDX19arTEtohhSaz+Zfd2AhNhb9N1+j0x9E8Sq+2Obr325+Ay22tX5xNNUo9FiKt7y5xBE20+Vh2nEavzsGAcq1SiZcW35AUVMF2nr8RoUHIc09JwqgQWXK8KUJk52iJkNkAbTXx4S1ZIWAivZ+q+zn7Dl0GJOuWo5s9OSaRJpWXOmcQ2racofkeMUNdUHipLjDtGqJpons+gYbsCdWSV/abm1YQH1xWEH+7WGH/qEjO/IyJf0QCk9Z3FkhviY1Qj1lFCzpcgJC41YsMgCAaBwporj01dwN4NwO20Ze9GdSlfubvXi4LB2ggBezeaimnAyKjCT/DvmeC72y+VZxcslt/X7JdwO3a8NYD3c//7mv0SwR322SDnSetdIEPdxiMaAJdr3EWiRSyLO2z5wser4Pu6/Qr+PZQivsw6bR1O+/nNP2DG9Ued2tpJd7vLBgBgHOa613Y7ahlBDFGNmxPjht1GE1jKzXRsxI5+j5tre7iP0nVqiyWc7607yvA9JCaumC21tpi37bLF91EevC1We/yByPyWRYZb3F3gn66K66xTCEzsabu7Md7GZX8FZFdr5zlXz8+4c9UlzgK3UxF+/wvAyNMm0pMn3t7pHNjIOk1vb413BL7OQG+LXqv7AdwR3NWkTaTf8z/CCywrffLiPOV+fo3AKNb2+7hp1KdTMLuXbDX1Ed67Qm2Bj8jjd92s+htqEYzUONFE9odGMozWyi1td7iE7cbgNIpJTWz3Z4hxIgc1J7OGxffnfAuZGXlnYM33wV0fhP4WHuVOhruPiCJwq9ri2+vLrzE8PMTWm2KzQjON4ZeY3sMpcEkM1ZOXVnjZ1oo8xIOMYMWtrWcx/JbY9EH2n8c4rKrUDqYPoDsQqPzzJzf8EivyGMzY5GX0fW1/jIdpijzHITt9X+GfY/UQWhF27tEiLy0goQ9imbEOkTmtIT0OHsNBKXbObC/MQki3axd/Dl4LWSdZMVU/eH+Fc52lpF6JeITKY+iOCsKbj+tuj0W85IoLHhjoYQd8pzDE98Bi2yBUBKmzddY5lfvtNHcXsYEa5PGPHH58CMli5CF6lwHR7tB4egInyPx153piY7emoyvhknW5gL1x5osy/eI+3BO8St5GN6vmqSy+hOxNubFhzWv+iFEN3p94VqLafBdS6IurwUC+R1K134+yn7cdL1P/T8Bk8G5Wk72WRbM+gGZY7Rqt7c7iPsQc0wBrV01JSMmVxaMYXaxL4AznDrnV+qOfAnsf1sXMxea7VjQTw1njHjoQ2XiggYw1rbRhBSP/HDcRY0Wnxm2ErBOF7JhHYSZT5i8EZnO/dyOx6Nq8dIAeSTfisz3UNIkt4obDMZpeUhJqZ/dQTwLGaHNlNyrY+Nw4EnjFy6aGaPNdkcnNFsP9AHwv8kYJWObGworY+LVBwkLvP0yQHqxdD254o/Y4kGoVk3bK6POK88e21g8iLpHLFV9bIsy0B7kYmDBK6zXB56c4j6MVmXkUN919Af9yr3o9+HQB51FGaGGmXyfmioOMU1DQ1CcFgofpY6Cdr+2Kr+2trZ2C8terxLKHkRhwW12r57BhWT08Wfua4/h2a5+/Bha2+rhxUwib20gNAiByF+AGctPH0R0IXFWLyy0gbKHkGrqMSFayLXM/SXgbPpaDAPMcnQZi8HCBJmKQX2ZGaPjjF6wV2MCPQwwhOm2atfAdyZvnY3xfgcuW+BqSuyQ5XwdrXBcTERs2L2607plQcssym6It8lTxg3SyeunoZWjCdumVZB3ufgtejMcTsb/vI2l7PYLIZB14hbLE5DV0KDo3OiTATRWdPTh8MVC7Od2UwpeAscdL95al9P4mJ1/de/ZNMdOerj0/hOI2PBKtajpCirXM7H5bRd+sk4Jdt7KjdAG9bEP62NnSrLLRIPK+v/VgYrI/NIyiENxu5BEdwQJYKbQfdBTh7SfqTNL1znz/GcK6IlWU6PsTSMW/UhmP1b10QBiQB/1/1F/kkFU94aWDh5j8HnVcDNe3/weNEeQldCrueB/WHj169OjRo0ePHj169OjRo0ePHj169Ojxf4TKsS4/Ya1Mfi+/wNr7+iztIC2oJANMas1H9avV1/Cvy3ASESEDDHkXuNrZSy2xkREErEImwvkOX1VLceRSJbkuQgZgYDckLHKa4XdQtVoHVctjul1Vy6/kpVRcH5gCDRFKkSEHQZO/D0cixiWeE60S3ogMTijh6fElrF9KK+t4tTBsalF6ghJG6wxlgXxEkqxdALvkiFoNDoK1rJZIryEiis6/j/3JGqFsw88czdjHpYPiFXvJylOAivI0KMr9YsleTwHqdvEpprHnJYVHCJMapf6ABm6M+F4AhHqExhQfRxR5HmVHepQR8+AJlB47gRdSfpAnHtnEKnoc4V003rywgnCXIDeg5cCN2Is/pmWQRN5uzI6bBUnuJXm3G5545WodJ345y0rPm6anj+NqcqAHf1WMl6vgMHWy3WlbTKcfRTZNttEs8/k+setyFY2TxC/c6Wab7MYY0SxmUsso8qOQTgiKk7hEhResisIb+bGTDlaOT/E6RYj9SOLRQ9Rt9lt4KjyvzGkSJNSbxocQjbcomKSbNN+RYhN6aDtAWwelByfFXjTxc1adosxIdhgj32HieIlHLkE0WUPDilYUoT2hxzFK2G1zVzF78Yv0cJyNXIoiB6GPkMxmzrUFMS0Bk3C7LhLWBkfsZVUcckYMO36ex+MVImHq4O0AHwYo/3DZLY5OPn/SdshqH/sFSmfHEO1x4DI1mcGG/OOEJ+AvSP4y9V/XBSZuGrMWOpsWZDv1KWISG5cYxeFu3XGSWBHHZc5axmqwKfJd7Ec4mnn5jpXnJYpjb1ds1zR1kTtbu2Q8cMdTJpF4k5NgFpBNOmPE2FvQIXzH+wQyHvaYeHH2Ufg5LVP2snHKnJTOasyuhU78Mb1R0vXq0zhZBt7Bo7NssFxti2iZxsmUrBcfRTgTSi7fHam7XyFvtnDyI3KCgYvCj1MeTxcp2WboAw346iKuK+Ppab8YRcvlcsA64gGFm8kI5fwl3CwcBLpw7PPb+QE3oMumePbX+Ip8eQTW/5DzmDmuUmOrwRGpj0QMJ93mBKtFvSJVDSnrgUhbpDpAthwsrQxYrUmI8TQCZOxIS1BVKIZ0GPdINZb/KN3gVyDC+OG5M4KPfFdVEB7PJw0ksKLE0VjYJuo+VJyQtMWkbQbml/wCnr9QDey465wIaQBKu67KJpIsSGUEgs0oU4WwzHpVa7SVUSWX8REpECQYV0eJQnXeP+8t/j/r4fmjGRp2rQAAAABJRU5ErkJggg=='} alt='imagen'/> */}
                            1
                            <img src='public/vite.svg' alt='imagen'/>
                        </ Link>
                    </Nav>
                    </Navbar.Collapse>
                </Container>
            </Navbar>
            {/* { homeContainer({saludo: 'Soy saludo de navbar'}) } */}
        </>
    )
}

export default NavBar


