import { BrowserRouter, Navigate, Route, Routes } from 'react-router-dom'

import NavBar from './components/NavBar/NavBar'
import ItemListContainer from './containers/ItemListContainer/ItemListContainer'
import ItemDetailContainer from './containers/ItemDetailContainer/ItemDetailContainer'
import CartContainer from './containers/CartContainer/CartContainer'

import 'bootstrap/dist/css/bootstrap.min.css';





// porque es un compnente funciones que retornan elementos  <- jsx 
function App() { 
  // definido en app
    let saludo  = 'hola Fede sos el mejor HomeContainer'
    let saludar = () => alert('soy func de app') //estados
    // console.log('Rering App')
    return (

        <div 
            // onClick={() => alert('Soy onClick de app')}
            // className="border border-5 border-primary w-100" 
        >
            <BrowserRouter>  
                <NavBar  
                    // homeContainer={ HomeContainer } 
                />   
                <Routes >
                    <Route path='/' element={ <ItemListContainer saludo={saludo} /> } />
                    <Route path='/categoria/:categoriaId' element={ <ItemListContainer saludo={saludo} /> } />
                    
                    <Route path='/detail/:productoId' element={ <ItemDetailContainer /> } />
                    <Route path='/cart' element={<CartContainer />} />                
                    {/* <Route path='/notpage' element={<Componente404 />} />                 */}
                    
                    <Route path='*' element={<Navigate to='/' />}/>
                </Routes>          
                
            </BrowserRouter>       

        </div>
    )
}

export default App



  

